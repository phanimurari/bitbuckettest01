import './index.css'

const PasswordIem = props => {
  const {passwordDetails, deletePasswordItem, showPasswords} = props
  const {
    id,
    website,
    username,
    password,
    initialContainerClassName,
  } = passwordDetails
  const starsImageClassName = showPasswords ? 'stars-none' : ''
  const passwordClassName = showPasswords ? 'show-passwords' : ''
  const initial = website ? website[0].toUpperCase() : ''

  const onDeletePasswordItem = () => {
    deletePasswordItem(id)
  }

  return (
    <li className="password-item">
      <div className={`${initialContainerClassName} initial-container`}>
        <p className="initial">{initial}</p>
      </div>
      <div className="website-and-password">
        <p className="website">{website}</p>
        <p className="username">{username}</p>
        <img
          src="https://assets.ccbp.in/frontend/react-js/password-manager-stars-img.png"
          alt="stars"
          className={`${starsImageClassName} stars-img`}
        />
        <p className={`${passwordClassName} password`}>{password}</p>
      </div>
      <button
        type="button"
        className="delete-button"
        onClick={onDeletePasswordItem}
        testid="delete"
      >
        <img
          src="https://assets.ccbp.in/frontend/react-js/password-manager-delete-img.png"
          alt="delete"
          className="delete-image"
        />
      </button>
    </li>
  )
}

export default PasswordIem
