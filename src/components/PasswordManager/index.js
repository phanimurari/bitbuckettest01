import {Component} from 'react'
import {v4} from 'uuid'

import PasswordItem from '../PasswordItem'

import './index.css'

const initialContainerBackgroundClassNames = [
  'amber',
  'blue',
  'orange',
  'emerald',
  'teal',
  'red',
  'light-blue',
]

class PasswordManager extends Component {
  state = {
    passwordsList: [],
    websiteInput: '',
    usernameInput: '',
    passwordInput: '',
    searchInput: '',
    showPasswords: false,
  }

  deletePasswordItem = id => {
    const {passwordsList} = this.state
    const updatedPasswordsList = passwordsList.filter(
      eachPasswordItem => eachPasswordItem.id !== id,
    )
    this.setState({
      passwordsList: updatedPasswordsList,
    })
  }

  renderPasswordsList = searchResults => {
    const {showPasswords} = this.state

    return (
      <ul className="password-list">
        {searchResults.map(eachPasswordItem => (
          <PasswordItem
            key={eachPasswordItem.id}
            passwordDetails={eachPasswordItem}
            deletePasswordItem={this.deletePasswordItem}
            showPasswords={showPasswords}
          />
        ))}
      </ul>
    )
  }

  renderNoPasswordsView = () => (
    <div className="no-passwords-view-container">
      <img
        src="https://assets.ccbp.in/frontend/react-js/no-passwords-img.png"
        alt="no passwords"
        className="no-passwords-img"
      />
      <p className="no-passwords">No Passwords</p>
    </div>
  )

  onChangeShowPasswords = event => {
    const {checked} = event.target
    this.setState({showPasswords: checked})
  }

  onChangeSearchInput = event => {
    this.setState({searchInput: event.target.value})
  }

  renderPasswords = () => {
    const {passwordsList, searchInput} = this.state
    const searchResults = passwordsList.filter(eachPasswordItem =>
      eachPasswordItem.website
        .toLowerCase()
        .includes(searchInput.toLowerCase()),
    )
    const passwordsCount = searchResults.length

    return (
      <div className="password-list-container">
        <div className="password-list-header-container">
          <div className="heading-and-count-container">
            <h1 className="your-passwords-heading">Your Passwords</h1>
            <p className="passwords-count">{passwordsCount}</p>
          </div>
          <div className="search-input-container">
            <div className="search-image-container">
              <img
                src="https://assets.ccbp.in/frontend/react-js/password-manager-search-img.png"
                alt="search"
                className="search-image"
              />
            </div>
            <input
              type="search"
              placeholder="Search"
              value={searchInput}
              onChange={this.onChangeSearchInput}
              className="search-input"
            />
          </div>
        </div>
        <hr className="horizontal-line" />
        <div className="show-passwords-container">
          <input
            className="show-passwords-checkbox"
            id="showPasswords"
            type="checkbox"
            onChange={this.onChangeShowPasswords}
          />
          <label className="show-passwords-label" htmlFor="showPasswords">
            Show Passwords
          </label>
        </div>
        {searchResults.length === 0
          ? this.renderNoPasswordsView()
          : this.renderPasswordsList(searchResults)}
      </div>
    )
  }

  onChangePasswordInput = event => {
    this.setState({passwordInput: event.target.value})
  }

  onChangeUsernameInput = event => {
    this.setState({usernameInput: event.target.value})
  }

  onChangeWebsiteInput = event => {
    this.setState({websiteInput: event.target.value})
  }

  onAddPassword = event => {
    event.preventDefault()
    const {websiteInput, usernameInput, passwordInput} = this.state
    const initialBackgroundColorClassName =
      initialContainerBackgroundClassNames[
        Math.ceil(
          Math.random() * initialContainerBackgroundClassNames.length - 1,
        )
      ]

    const newPassword = {
      id: v4(),
      website: websiteInput,
      username: usernameInput,
      password: passwordInput,
      initialContainerClassName: initialBackgroundColorClassName,
    }

    this.setState(prevState => ({
      passwordsList: [...prevState.passwordsList, newPassword],
      websiteInput: '',
      usernameInput: '',
      passwordInput: '',
    }))
  }

  renderAddNewPasswordForm = () => {
    const {websiteInput, usernameInput, passwordInput} = this.state

    return (
      <form className="add-new-password-form" onSubmit={this.onAddPassword}>
        <h1 className="add-new-password-heading">Add New Password</h1>
        <div className="input-container">
          <div className="logo-container">
            <img
              src="https://assets.ccbp.in/frontend/react-js/password-manager-website-img.png"
              alt="website"
              className="logo-image"
            />
          </div>
          <input
            type="text"
            value={websiteInput}
            onChange={this.onChangeWebsiteInput}
            className="input"
            placeholder="Enter Website"
          />
        </div>
        <div className="input-container">
          <div className="logo-container">
            <img
              src="https://assets.ccbp.in/frontend/react-js/password-manager-username-img.png"
              alt="username"
              className="logo-image"
            />
          </div>
          <input
            type="text"
            value={usernameInput}
            onChange={this.onChangeUsernameInput}
            className="input"
            placeholder="Enter Username"
          />
        </div>
        <div className="input-container">
          <div className="logo-container">
            <img
              src="https://assets.ccbp.in/frontend/react-js/password-manager-password-img.png"
              alt="password"
              className="logo-image"
            />
          </div>
          <input
            type="password"
            value={passwordInput}
            onChange={this.onChangePasswordInput}
            className="input"
            placeholder="Enter Password"
          />
        </div>
        <button type="submit" className="add-button">
          Add
        </button>
      </form>
    )
  }

  render() {
    return (
      <div className="password-manager-container">
        <div className="password-manager-responsive-container">
          <img
            src="https://assets.ccbp.in/frontend/react-js/password-manager-logo-img.png"
            alt="app logo"
            className="app-logo"
          />
          <div className="password-manager">
            {this.renderAddNewPasswordForm()}
            <img
              src="https://assets.ccbp.in/frontend/react-js/password-manager-sm-img.png"
              alt="password manager"
              className="password-manager-sm-image"
            />
            <img
              src="https://assets.ccbp.in/frontend/react-js/password-manager-lg-img.png"
              alt="password manager"
              className="password-manager-lg-image"
            />
          </div>
          {this.renderPasswords()}
        </div>
      </div>
    )
  }
}

export default PasswordManager
